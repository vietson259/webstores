var express = require('express');
var router = express.Router();
const NodeCouchDb = require("node-couchdb");
const couch = new NodeCouchDb({
  protocol: "https",
  host: "e3ac9fef-2e41-4131-9789-209f09aa5044-bluemix.cloudantnosqldb.appdomain.cloud",
  port: 443,
  auth: {
    user: "e3ac9fef-2e41-4131-9789-209f09aa5044-bluemix",
    pass: "e3f506df19bd4921c99a958f6f46dfb5c2bf44a967eca323904ee294095fc216"
  }
});

router.get('/', async (req, res) => {
  let query = {
    selector: {
      type: {
        $eq: "q"
      }
    },
    limit: 200
  };
  let docs = await couch.mango("tracnghiemdb", query).then(res => { return res.data.docs; });
  let vm = {
    question: docs
  };
  console.log(docs);
  res.render('home/about', vm);
});

module.exports = router;