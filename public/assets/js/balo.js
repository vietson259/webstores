jQuery(document).ready(function($) {
	var sIn = "hide", sOf = "hide";
	$('#signIn').click(function(){
		$('#sIn').removeClass("display-none");
		$('.main-body').addClass("hide_");
		$('header').addClass("hide_");
	});

	$('#btnCancelSignIn').click(function(){
		$('#sIn').addClass("display-none");
		$('.main-body').removeClass("hide_");
		$('header').removeClass("hide_");
	});

	$('#btnCancelSignOf').click(function(){
		$('#sOf').addClass("display-none");
		$('.main-body').removeClass("hide_");
		$('header').removeClass("hide_");
	});

	$('#btnSignIn').click(function(){
		$('#sIn').addClass("display-none");
		$('#signOut').removeClass('hide');
		$('#signIn').addClass('hide');
		$('.main-body').removeClass("hide_");
		$('header').removeClass("hide_");
	});

	$('#formSignOf').click(function(){
		$('#sIn').addClass("display-none");
		$('#sOf').removeClass("display-none");
	});

	$('#formSignIn').click(function(){
		$('#sOf').addClass("display-none");
		$('#sIn').removeClass("display-none");
	});

	$('#btnSignOf').click(function(){
		$('#sOf').addClass("display-none");
		$('#signOut').removeClass('hide');
		$('#signIn').addClass('hide');
		$('.main-body').removeClass("hide_");
		$('header').removeClass("hide_");
	});

	$('#signOut').click(function(){
		$(this).addClass('hide');
		$('#signIn').removeClass('hide');
	});

	$('#menu-brand').on('click', function(event){
		event.preventDefault();
		$("html,body").animate({scrollTop: $('#brand-products').offset().top - 100}, "slow");
	});

	$('.toTop').on('click', function(event){
		event.preventDefault();
		$("html,body").animate({scrollTop: 0}, "slow");
	});

	$('#info-product-detail').on('click', function(){
		event.preventDefault();
		$('.info-product-detail').removeClass('hide');
		$('.detail-number').addClass('hide');
	});

	$('#number-tech').on('click', function(){
		event.preventDefault();
		$('.detail-number').removeClass('hide');
		$('.info-product-detail').addClass('hide');
	});
});
