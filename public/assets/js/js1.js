$(function() {

	$('.main-menu li:nth-child(3)').on('click', function(event){
		event.preventDefault();
		$("html,body").animate({scrollTop: $('.new-products').offset().top - 40}, "slow");
	});

	$('.main-menu li:nth-child(4)').on('click', function(event){
		event.preventDefault();
		$("html,body").animate({scrollTop: $('.common-products').offset().top - 40}, "slow");
	});

	$('.main-menu li:nth-child(5)').on('click', function(event){
		event.preventDefault();
		$("html,body").animate({scrollTop: $('.best-seller-products').offset().top - 40}, "slow");
	});

	$('.main-menu li:nth-child(6)').on('click', function(event){
		event.preventDefault();
		$("html,body").animate({scrollTop: $('.brand-products').offset().top - 50}, "slow");
	});

	configOwlCarousel = {
		loop: true,
		center: true,
		items: 5,
		margin: 30,
		autoplay: true,
		dots:true,
		nav:true,
		autoplayTimeout: 8500,
		smartSpeed: 450,
		navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 1
			},
			1170: {
				items: 5
			}
		}
	}

	configOwlCarouselDetail = {
		loop: true,
		center: true,
		items: 5,
		margin: 30,
		autoplay: true,
		dots:true,
		nav:true,
		autoplayTimeout: 8500,
		smartSpeed: 450,
		navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 1
			},
			1170: {
				items: 1
			}
		}
	}
	$('#common-products').owlCarousel(configOwlCarousel);
	$('#best-sell-products').owlCarousel(configOwlCarousel);
	$('#new-products').owlCarousel(configOwlCarousel);
	$('#img-item-info').owlCarousel(configOwlCarouselDetail);
	$('.tuixach').on('click', function(event){
		$('#best-sell-products').owlCarousel(configOwlCarousel);
	});
	$('#index').on('click', function(){
		$('#common-products').owlCarousel(configOwlCarousel);
		$('#best-sell-products').owlCarousel(configOwlCarousel);
		$('#new-products').owlCarousel(configOwlCarousel);
	});
});