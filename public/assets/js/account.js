$(function() {
	$('.toTop').on('click', function(event){
		event.preventDefault();
		$("html,body").animate({scrollTop: 0}, "slow");
	});

	$('#signIn').click(function(){
		$('#sIn').removeClass("display-none");
		$('.main').addClass("hide_");
		$('header').addClass("hide_");
	});

	$('#btnCancelSignIn').click(function(){
		$('#sIn').addClass("display-none");
		$('.main').removeClass("hide_");
		$('header').removeClass("hide_");
	});

	$('#btnCancelSignOf').click(function(){
		$('#sOf').addClass("display-none");
		$('.main').removeClass("hide_");
		$('header').removeClass("hide_");
	});

	$('#btnSignIn').click(function(){
		$('#sIn').addClass("display-none");
		$('#signOut').removeClass('hide');
		$('#signIn').addClass('hide');
		$('.main').removeClass("hide_");
		$('header').removeClass("hide_");
		$('#menu-account').removeClass('hide');
	});

	$('#formSignOf').click(function(){
		$('#sIn').addClass("display-none");
		$('#sOf').removeClass("display-none");
	});

	$('#formSignIn').click(function(){
		$('#sOf').addClass("display-none");
		$('#sIn').removeClass("display-none");
	});

	$('#btnSignOf').click(function(){
		$('#sOf').addClass("display-none");
		$('#signOut').removeClass('hide');
		$('#signIn').addClass('hide');
		$('.main').removeClass("hide_");
		$('header').removeClass("hide_");
		$('#account').removeClass('hide');
		$('#menu-account').removeClass('hide');
	});

	$('#signOut').click(function(){
		$(this).addClass('hide');
		$('#signIn').removeClass('hide');
		$('#account').addClass('hide');
		$('#menu-account').addClass('hide');
	});

	$('#menu-account').click(function(){
		$('#info-account').removeClass("display-none");
		$('.main').addClass("hide_");
		$('header').addClass("hide_");
	});

	$('#btnCancelInfoAccount').click(function(){
		$('#info-account').addClass("display-none");
		$('.main').removeClass("hide_");
		$('header').removeClass("hide_");
	});

	$('#formInfoAccount').click(function(){
		$('#info-account').addClass("display-none");
		$('#mod-account').removeClass("display-none");
	});

	$('#formModAccount').click(function(){
		$('#info-account').removeClass("display-none");
		$('#mod-account').addClass("display-none");
	});

	$('#btnCancelModAccount').click(function(){
		$('#mod-account').addClass("display-none");
		$('.main').removeClass("hide_");
		$('header').removeClass("hide_");
	});

	$('#btnModAccount').click(function(){
		$('#mod-account').addClass("display-none");
		$('.main').removeClass("hide_");
		$('header').removeClass("hide_");
	});

});