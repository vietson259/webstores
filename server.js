var express = require("express"),
    app = express(),
    exphbs = require('express-handlebars'),
    express_handlebars_sections = require('express-handlebars-sections');

var port = process.env.PORT || 8080;
var homeController = require('./controllers/homeController');

app.use(express.static(__dirname + '/public'));

app.engine('hbs', exphbs({
  defaultLayout: 'main',
  layoutsDir: 'views/_layouts/',
  helpers: {
      section: express_handlebars_sections(),
      number_format: n => {
          var nf = wnumb({
              thousand: ','
          });
          return nf.to(n);
      }
  }
}));
app.set('view engine', 'hbs');

app.get("/sayHello", function (request, response) {
  var user_name = request.query.user_name;
  response.end("Hello " + user_name + "!");
});

app.use('/home', homeController);

app.listen(port, () => {
  console.log("Listening on port ", port);
});

require("cf-deployment-tracker-client").track();
